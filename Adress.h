#pragma once

#include<iostream>

using namespace std;

class Adress {
	string ulica;
	string numer;
	string miasto;
	string kod_pocztowy;
public:
	Adress();
	Adress(string nazwa_ulicy, string numer_domu, string miasto, string kod_pocztowy);

	void setUlica(string);
	void setNumer(string);
	void setMiasto(string);
	void setKod_Pocztowy(string);

	string getUlica();
	string getNumer();
	string getMiasto();
	string getKod_Pocztowy();

};