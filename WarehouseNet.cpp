#include "WarehouseNet.h"

WarehaouseNet::WarehaouseNet()
{
	 Nr_Hurtowni = "Numer hurtowni";
	 Produkty = "Nazwa produktu";
	 Pow_Magazynow = 0.0;
	 Stan_Magazynow = 0.0;
}

WarehaouseNet::WarehaouseNet(string nr_hurtowni, string produkty, double pow_magazynow, double stan_magazynow)
{
	Nr_Hurtowni = nr_hurtowni;
	Produkty = produkty;
	Pow_Magazynow = pow_magazynow;
	Stan_Magazynow = stan_magazynow;
}

void WarehaouseNet::setNr_Hurtowni(string nrw) { Nr_Hurtowni = nrw; }
void WarehaouseNet::setProdukty(string prod) { Produkty = prod; }
void WarehaouseNet::setPow_Magazynow(double warea) { Pow_Magazynow = warea; }
void WarehaouseNet::setStan_Magazynow(double wstate) { Stan_Magazynow = wstate; };
void WarehaouseNet::setAdress(Adress adh) { adresHurt = adh; }

string WarehaouseNet::getNr_Hurtowni(){return Nr_Hurtowni;}
string WarehaouseNet::getProdukty(){return Produkty;}
double WarehaouseNet::getPow_Magazynow(){return Pow_Magazynow;}
double WarehaouseNet::getStan_Magazynow(){return Stan_Magazynow;}
Adress WarehaouseNet::getAdress() { return adresHurt; }
