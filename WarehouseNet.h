#pragma once

#include "Adress.h"
#include<iostream>

using namespace std;

class WarehaouseNet {
	string Nr_Hurtowni;
	string Produkty;
	double Pow_Magazynow;
	double Stan_Magazynow;
	Adress adresHurt;
public:
	WarehaouseNet();
	WarehaouseNet(string nr_hurtowni, string produkty, double pow_magazynow, double stan_magazynow);

	void setNr_Hurtowni(string);
	void setProdukty(string);
	void setPow_Magazynow(double);
	void setStan_Magazynow(double);
	void setAdress(Adress);

	string getNr_Hurtowni();
	string getProdukty();
	double getPow_Magazynow();
	double getStan_Magazynow();
	Adress getAdress();
};