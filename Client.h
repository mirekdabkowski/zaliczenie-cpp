#pragma once
#include<iostream>
#include "Adress.h"

using namespace std;

class Client {
	Adress adress;
	string imie;
	string nazwisko;

public:
	Client();
	Client(string client_name, string client_surname);

	void setAdress(Adress);
	void setImie(string);
	void setNazwisko(string);

	Adress getAdress();
	string getImie();
	string getNazwisko();
};