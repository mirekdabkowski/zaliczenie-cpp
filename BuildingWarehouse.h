#pragma once


#include "WarehouseNet.h"
#include "Client.h"
#include<iostream>

using namespace std;

class BiuildingWarehouse {
	string Wlasciciel;
	double Przyjecie_prod;
	double Wydanie_prod;
	
public:
	BiuildingWarehouse();
	BiuildingWarehouse(string wlasciciel, double przyjecie, double wydanie);

	void setWlascieciel(string);
	void setPrzyjecie_prod(double);
	void setWydanie_prod(double);
	
	
	string getWlasciciel();
	double getPrzyjecie_prod();
	double getWydanie_prod();
	
};